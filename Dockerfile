FROM debian:unstable-slim

RUN apt-get update \ 
	&& apt-get install -y --no-install-recommends lintian \
	&& rm -rf /var/lib/apt/lists/*
